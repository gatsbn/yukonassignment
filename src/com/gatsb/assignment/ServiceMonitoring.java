package com.gatsb.assignment;

import java.util.Date;
import java.util.Timer;
import java.util.TimerTask;

/**
 * Created by Gatsb on 12.05.2016.
 */

public class ServiceMonitoring {

    private int port;
    private String host;

    public ServiceMonitoring(int port, String host){
        this.port = port;
        this.host = host;
    }

    public int getPort() {
        return port;
    }

    public void setPort(int port) {
        this.port = port;
    }

    public String getHost() {
        return host;
    }

    public void setHost(String host) {
        this.host = host;
    }

    public void planOutage(Date start, Date end){
        final ServiceMonitoring serviceMonitoring = new ServiceMonitoring(port,host);
            final Timer timer = new Timer();
            timer.schedule((new TimerTask() {
                @Override
                public void run() {
                    serviceMonitoring.suspendNotification();
                }
            }),start);
        }

    public void suspendNotification(){
        System.out.println("No notification will be delivered");
    }
}
