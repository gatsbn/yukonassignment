package com.gatsb.assignment;

import java.net.Socket;
import java.util.Calendar;
import java.util.Date;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

/**
 * Created by Gatsb on 12.05.2016.
 */
public class Monitor {

    ServiceMonitoring serviceMonitoring;
    private int pollingFrequency;
    private Date graceTime;

    public Monitor(ServiceMonitoring serviceMonitoring, Date graceTime){
        this.serviceMonitoring = serviceMonitoring;
        this.graceTime = graceTime;
    }

    public Long timeToExpire(Date date){
        Long expireTime = null;
        Calendar calendar = Calendar.getInstance();
        Date currentTime = calendar.getTime();
        if(currentTime.after(date)){
            expireTime = currentTime.getTime() - date.getTime();
        }
        return expireTime;
    }

    public void notifyCaller(){
        System.out.println(serviceMonitoring.getHost()+ " " + serviceMonitoring.getPort() + " caller's been notified");
    }

    public void checkIfServiceUp(final ServiceMonitoring serviceMonitoring){
        ExecutorService executorService = Executors.newCachedThreadPool();
        executorService.submit(new Callable<Void>() {
            public Void call() {
                Socket socket = null;
                try {
                    socket = new Socket(serviceMonitoring.getHost(),serviceMonitoring.getPort());
                    if (socket.isConnected()){
                        notifyCaller();
                        socket.close();
                    }
                    if (graceTime!=null & !(socket.isConnected())){
                        try {
                            socket.wait(timeToExpire(graceTime));
                        } catch (InterruptedException e) {
                            e.printStackTrace();
                        }
                    }
                } catch (Exception e){
                    System.out.println(serviceMonitoring.getHost() + " " + serviceMonitoring.getPort() + " is not up");
                }
                return null;
            }
        });
        executorService.shutdown();
    }

    public void RegisterInterest(ServiceMonitoring serviceMonitoring, int pollingFrequency){
        this.pollingFrequency = pollingFrequency;
        while (pollingFrequency > 0){
            System.out.println("Time remaining in seconds: " + pollingFrequency);
            checkIfServiceUp(serviceMonitoring);
            pollingFrequency--;
        }
    }
}
