package com.gatsb.assignment;

import org.junit.*;
import org.mockito.Mockito;

import java.util.Calendar;
import java.util.Date;

import static org.junit.Assert.*;

/**
 * Created by Gatsb on 15.05.2016.
 */
public class ServiceMonitoringTest {

    ServiceMonitoring serviceMonitoring = new ServiceMonitoring(3128, "localhost");
    String host = serviceMonitoring.getHost();
    int port = serviceMonitoring.getPort();

    @BeforeClass
    public static void setUpBeforeClass() throws Exception {
    }

    @AfterClass
    public static void tearDownAfterClass() throws Exception {
    }

    @Before
    public void setUp() throws Exception {

    }

    @After
    public void tearDown() throws Exception {

    }

    @Test
    public void getPort() throws Exception {
        assertTrue(port == 3128);
    }

    @Test
    public void setPort() throws Exception {
        serviceMonitoring.setPort(20);
        assertTrue(serviceMonitoring.getPort() == 20);
    }

    @Test
    public void getHost() throws Exception {
        assertTrue(serviceMonitoring.getHost() == "localhost");
    }

    @Test
    public void setHost() throws Exception {
        ServiceMonitoring serviceMonitoring = new ServiceMonitoring(3128, "localhost");
        serviceMonitoring.setHost("www.google.com");
        assertEquals("www.google.com", serviceMonitoring.getHost());
    }

    @Test
    public void planOutage() throws Exception {
        Calendar calendar = Calendar.getInstance();
        calendar.getTime();
        Calendar calendar1 = Calendar.getInstance();
        calendar.getTime();
        calendar1.set(Calendar.MINUTE, Calendar.MINUTE + 10);
        Date start = calendar.getTime();
        Date end = calendar1.getTime();

        ServiceMonitoring serviceMonitoring = Mockito.mock(ServiceMonitoring.class);
        serviceMonitoring.planOutage(start, end);
        Mockito.verify(serviceMonitoring).planOutage(start, end);
    }

}