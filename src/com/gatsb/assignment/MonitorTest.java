package com.gatsb.assignment;

import org.junit.*;
import org.mockito.Mockito;

/**
 * Created by Gatsb on 15.05.2016.
 */
public class MonitorTest {

    @BeforeClass
    public static void setUpBeforeClass() throws Exception {
    }

    @AfterClass
    public static void tearDownAfterClass() throws Exception {
    }

    @Before
    public void setUp() throws Exception {

    }

    @After
    public void tearDown() throws Exception {

    }

    @Test
    public void checkIfServiceUp() throws Exception {
        Monitor monitor = Mockito.mock(Monitor.class);
        ServiceMonitoring serviceMonitoring = new ServiceMonitoring(80, "localhost");
        monitor.checkIfServiceUp(serviceMonitoring);
        monitor.notifyCaller();
        Mockito.verify(monitor).checkIfServiceUp(serviceMonitoring);
    }

}